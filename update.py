#!/usr/bin/env python3

import json
import yaml
import os

users = list()

for root, dirs, files in os.walk('developers'):
    for f in files:
        if not f.endswith('.yaml'):
            continue

        username = f[:-5]
        userinfo = yaml.load(open(os.path.join(root, f)))
        if not 'locations' in userinfo:
            continue

        userinfo['type'] = 'developer'

        if not 'email' in userinfo:
            userinfo['email'] = f'{username}@debian.org'

        users.append(userinfo)

with open('www/users.json', 'w') as usersjson:
    json.dump(sorted(users, key=lambda u: u['name'] if 'name' in u else ''), usersjson)
